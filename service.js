"use strict";
const DbService = require("moleculer-db");
const SqlAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const conexion = {
    "database": "cecati",
    "username": "informatica",
    "pwd": "informatica",
    "config": {
        "dialect": "mysql",
        "host": "localhost"
    }
};
module.exports = {
	name: "diasInhabiles",

	mixins: [DbService],
	adapter: new SqlAdapter(conexion.database, conexion.username, conexion.pwd, conexion.config),
	model: {
		name: "dias_inhabiles",//Nombre de la tabla
        define: {
			dia: Sequelize.DATE
        },
        options: {
            // Options from http://docs.sequelizejs.com/manual/tutorial/models-definition.html
        }
    },
	settings: {
		onError(req, res, err) {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(err.code || 500);
            res.end(JSON.stringify({
                success: false,
                message: err.message
            }));
        }	

	},
	/**
	 * Actions
	 */
	actions: {

		addDia: {
			params: {
				dia: "string",
				
			},
			handler(ctx) {

				// se convierte la fecha que viene en string en un arreglo para convertir los numeros a enteros
				let paramsDias = ctx.params.dia.split("-");


				let anio = parseInt(paramsDias[0]);
				let mes  = parseInt(paramsDias[1]) -1;//se le resta un dia ya que en el objeto dateTime el mes empieza desde 0
				let dia  = parseInt(paramsDias[2]);

				

				let fecha = new Date(anio,mes,dia);// se comvierte los parametros en un objeto fecha


				console.log("paramsDias",paramsDias);
				console.log("paramsDias",fecha);



				//se crea un objeto para insertar en la BD
				let insert = {
					dia: fecha.toString(),//dia es el nombre de la columna de la tabla
				}

				//se inserta el objeto el la BD
				return this.adapter.insert(insert).then(result => {
					return result;
				});
			}
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}	
};